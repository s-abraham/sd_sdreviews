﻿using HtmlAgilityPack;
using SDJobLib;
using SDJobLib.JobDataObjects;
using SDJobLib.Proxy;
using SDJobLib.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ReviewDownload
{
    class Program
    {
        static void Main(string[] args)
        {
            fastJSON.JSON.Instance.Parameters.UseExtensions = false;

            List<long> jobIDs = new List<long>();
            if (args.Length == 1 && args[0].Length > 0)
            {
                foreach (string arg in args[0].Split(new char[] { ',' }))
                {
                    long jobID;
                    if (long.TryParse(arg, out jobID))
                        jobIDs.Add(jobID);
                }
            }

            //load jobs
            List<Job> jobs = SDJobs.GetJobs(jobIDs);

            //do the work
            foreach (Job job in jobs)
            {
                doWork(job);
            }
        }

        private static void doWork(Job job)
        {
            SDJobs.SetJobStatus(job.ID, JobStatusEnum.Processing);
            try
            {
                DealerRaterJobData data = fastJSON.JSON.Instance.ToObject<DealerRaterJobData>(job.JobData);
                dealerRaterDownload(job, data);
                SDJobs.SetJobStatus(job.ID, JobStatusEnum.Success);
            }
            catch (Exception ex)
            {
                SDJobs.SetJobFailed(job.ID, ex.Message, ex.ToString());
            }
        }

        #region dealer rater

        private static string _dealerRaterURL = null;
        private static string _dealerRaterToken = null;

        private static string _dealerRaterXPath_ParentNodeReviewSummary = null;
        private static string _dealerRaterXPath_ReviewCountCollected = null;
        private static int _dealerRaterPageSize = 10;

        private static void dealerRaterDownload(Job job, DealerRaterJobData data)
        {
            try
            {
                IDownloadClient client = new ProxyMeshDownloadClient();

                if (_dealerRaterURL == null)
                {
                    _dealerRaterURL = SDJobs.GetProcessorSetting("DealerRater.URL");
                    _dealerRaterToken = SDJobs.GetProcessorSetting("DealerRater.Token");

                    _dealerRaterXPath_ParentNodeReviewSummary = SDJobs.GetProcessorSetting("DealerRater.ParentnodeReviewSummaryXPath");
                    _dealerRaterXPath_ReviewCountCollected = SDJobs.GetProcessorSetting("DealerRater.ReviewCountCollectedXPath");

                    _dealerRaterPageSize = Convert.ToInt32(SDJobs.GetProcessorSetting("DealerRater.PageSize"));
                }

                #region initial api download try

                string url = data.URL;
                var dealerRaterid = url.Substring(url.LastIndexOf("-") + 1);
                if (dealerRaterid.Contains("page"))
                {
                    dealerRaterid = dealerRaterid.Substring(0, dealerRaterid.IndexOf("/"));
                    dealerRaterid = dealerRaterid.Replace("/", "");
                }
                else
                {
                    dealerRaterid = dealerRaterid.Replace("/", "");
                }

                string requestURL = string.Format("{0}/{1}?accessToken={2}", _dealerRaterURL, dealerRaterid, _dealerRaterToken);

                client.Download(requestURL, data.MaxRetries);

                if (client.LastException() != null)
                {
                    throw new Exception(string.Format("DealerRater API call failed for URL: {0} - Exception: {1}", requestURL, client.LastException()));
                }

                string content = client.StringResult();
                if (string.IsNullOrWhiteSpace(content))
                {
                    throw new Exception(string.Format("DealerRater API call returned empty result for URL: {0} - Exception: {1}", requestURL, client.LastException()));
                }

                #endregion

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        private static int dealerRaterGetReviewCount(HtmlDocument doc)
        {
            int ReviewCount = 0;
            string Xpath = string.Empty;
            try
            {


                Xpath = _dealerRaterXPath_ParentNodeReviewSummary;
                HtmlNode summaryNode = doc.DocumentNode.SelectSingleNode(_dealerRaterXPath_ParentNodeReviewSummary);

                Xpath = _dealerRaterXPath_ReviewCountCollected;
                ReviewCount = Convert.ToInt32(summaryNode.SelectSingleNode(_dealerRaterXPath_ReviewCountCollected).InnerText);

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("DealerRater XPath Failure for: {0} - {1}", Xpath, ex.ToString()));
            }

            return ReviewCount;

        }

        private static List<string> dealerRaterGetPageLinks(int reviewCount, string firstPageURL)
        {
            List<string> pageURLs = new List<string>();

            int pageCounter = 0;
            string URL = string.Empty;

            if (reviewCount > 0)
            {
                for (int iCounter = 0; iCounter < reviewCount; iCounter += _dealerRaterPageSize)
                {
                    pageCounter++;
                    if (iCounter != 0)
                    {
                        URL = string.Format("{0}page{1}/", firstPageURL, pageCounter);
                        pageURLs.Add(URL);
                    }
                }
            }

            return pageURLs;
        }

        #endregion
    }
}
