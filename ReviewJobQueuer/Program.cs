﻿using SDJobLib;
using SDJobLib.JobDataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReviewJobQueuer
{
    class Program
    {
        /// <summary>
        /// 
        /// Queues jobs to download reviews.  Those download jobs will spawn
        /// additional processing jobs upon completion.
        ///         
        /// Command line arguments:
        /// 
        /// -priority : sets the priority for the queued jobs (default 100)        
        /// -dlid : restricts to one or more DealerLocationIDs (comma delimited list, no spaces allowed, optional)
        /// -rsid : restricts to one or more ReviewSourceIDs (comma delimited list, no spaces allowed, optional)
        /// -dtype : sets the type of download to queue.  1=Summary+Detail, 2=Summary+Detail if Summary Changed, 3=Summary Only
        /// -mslc : selects only those urls with a minimum number of minutes since last completed, or those that never completed
        /// -mslq : selects only those urls with a minimum number of minutes since last queued, or those that never queued
        /// -nc : selects only those urls that have been queued, but never completed since the last time queued
        /// -ns : selects only those urls that have never been queued, or have been queued but subsequently completed
        /// -mret : maximum number of retries per download (default 0)
        /// 
        /// Example:
        /// 
        /// ReviewJobQueuer.exe -dtype 1 -dlid 5323,2544,5463 -ns -rsid 4,5 -mslc 1200
        /// 
        /// Queues Summary+Detail for DealerLocationIDs 5323,2544 and 5463 that are new URLs, or those that 
        /// completed successfully the last time queued, and only for ReviewsSourceIDs 4 and 5. 
        /// Only URLs that haven't completed within the last 20 hours will be queued.
        /// 
        /// </summary>        
        static void Main(string[] args)
        {
            try
            {
                string currentCommand = null;

                int priority = 100;
                DownloadTypeEnum type = DownloadTypeEnum.SummaryAndDetail;
                List<int> dealerLocationIDs = null;
                List<int> reviewSourceIDs = null;
                int? minutesSinceLastQueued = null;
                int? minutesSinceLastCompleted = null;
                bool onlyQueuedNotCompleted = false;
                bool onlyNewOrSuccessful = false;
                int maximumRetries = 0;
                
                #region argument parsing

                foreach (string arg in args)
                {
                    if (arg != null && arg.Trim().Length > 0 && arg.Substring(0, 1) == "-")
                    {
                        currentCommand = arg.Trim().Replace("-", "").ToLower();

                        switch (currentCommand)
                        {
                            case "nc":
                                onlyNewOrSuccessful = true;
                                break;
                            case "ns":
                                onlyQueuedNotCompleted = true;
                                break;

                            default:
                                break;
                        }
                    }
                    else if (arg != null && arg.Trim().Length > 0 && currentCommand != null)
                    {
                        switch (currentCommand)
                        {
                            case "mret":
                                int.TryParse(arg, out maximumRetries);
                                break;

                            case "priority":
                                int.TryParse(arg, out priority);
                                break;

                            case "dlid":
                                dealerLocationIDs = new List<int>();
                                foreach (string dlidString in arg.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                                {
                                    int dlid;
                                    if (int.TryParse(dlidString, out dlid))
                                    {
                                        dealerLocationIDs.Add(dlid);
                                    }
                                }
                                break;

                            case "rsid":
                                reviewSourceIDs = new List<int>();
                                foreach (string rsidString in arg.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                                {
                                    int rsid;
                                    if (int.TryParse(rsidString, out rsid))
                                    {
                                        reviewSourceIDs.Add(rsid);
                                    }
                                }
                                break;

                            case "dtype":
                                int dtype;
                                if (int.TryParse(arg, out dtype))
                                {
                                    switch (dtype)
                                    {
                                        case 1:
                                            type = DownloadTypeEnum.SummaryAndDetail;
                                            break;
                                        case 2:
                                            type = DownloadTypeEnum.SummaryAndDetailIfChanged;
                                            break;
                                        case 3:
                                            type = DownloadTypeEnum.SummaryOnly;
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                break;

                            case "mslc":
                                int mslc;
                                if (int.TryParse(arg, out mslc))
                                {
                                    minutesSinceLastCompleted = mslc;
                                }
                                break;

                            case "mslq":
                                int mslq;
                                if (int.TryParse(arg, out mslq))
                                {
                                    minutesSinceLastQueued = mslq;
                                }
                                break;

                            default:
                                break;
                        }
                    }
                }

                #endregion

                List<int> dealerLocationReviewIDs;
                if (dealerLocationIDs == null)
                {
                    if (reviewSourceIDs == null)
                    {
                        dealerLocationReviewIDs = SDJobs.GetReviewsToProcess(type, null, null, minutesSinceLastCompleted, minutesSinceLastQueued, onlyQueuedNotCompleted, onlyNewOrSuccessful);
                    }
                    else
                    {
                        dealerLocationReviewIDs = new List<int>();
                        foreach (int reviewSourceID in reviewSourceIDs)
                        {
                            dealerLocationReviewIDs.AddRange(SDJobs.GetReviewsToProcess(type, null, reviewSourceID, minutesSinceLastCompleted, minutesSinceLastQueued, onlyQueuedNotCompleted, onlyNewOrSuccessful));
                        }
                    }
                }
                else
                {
                    if (reviewSourceIDs == null)
                    {
                        dealerLocationReviewIDs = new List<int>();
                        foreach (int dealerLocationID in dealerLocationIDs)
                        {
                            dealerLocationReviewIDs.AddRange(SDJobs.GetReviewsToProcess(type, dealerLocationID, null, minutesSinceLastCompleted, minutesSinceLastQueued, onlyQueuedNotCompleted, onlyNewOrSuccessful));
                        }
                    }
                    else
                    {
                        dealerLocationReviewIDs = new List<int>();
                        foreach (int dealerLocationID in dealerLocationIDs)
                        {
                            foreach (int reviewSourceID in reviewSourceIDs)
                            {
                                dealerLocationReviewIDs.AddRange(SDJobs.GetReviewsToProcess(type, dealerLocationID, reviewSourceID, minutesSinceLastCompleted, minutesSinceLastQueued, onlyQueuedNotCompleted, onlyNewOrSuccessful));
                            }
                        }
                    }
                }

                queueDownloadJobs(dealerLocationReviewIDs, priority, type, maximumRetries);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        private static void queueDownloadJobs(List<int> dealerLocationReviewIDs, int priority, DownloadTypeEnum type, int maxRetries)
        {
            try
            {
                List<DealerLocationReview> dlrs = SDJobs.GetDealerLocationReviews(dealerLocationReviewIDs);
                List<BaseJobData> jobs = new List<BaseJobData>();

                foreach (DealerLocationReview dlr in dlrs)
                {
                    DealerRaterJobData job = new DealerRaterJobData()
                    {
                        DealerLocationReviewID = dlr.ID,
                        DealerLocationID = dlr.DealerLocationID,
                        ReviewSourceID = dlr.ReviewsSourceID,
                        URL = string.IsNullOrWhiteSpace(dlr.RequestURL) ? dlr.URL : dlr.RequestURL,
                        Type = type,
                        MaxRetries = maxRetries
                    };

                    jobs.Add(job);
                }

                SDJobs.SaveNewJobs(jobs, priority);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
    }
}
