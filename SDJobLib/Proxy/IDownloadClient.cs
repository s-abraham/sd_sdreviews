﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDJobLib.Proxy
{
    public interface IDownloadClient
    {
        /// <summary>
        /// Performs a download of the supplied URL
        /// </summary>        
        void Download(string url, int maxRetries);

        /// <summary>
        /// The string contents downloaded from the last call to Download.  Null if failed.
        /// </summary>
        string StringResult();

        /// <summary>
        /// Summary of an exception that was caught from the last call to Download.  Null if succeeded.
        /// </summary>
        /// <returns></returns>
        string LastException();

        /// <summary>
        /// The URL of the final redirection that resulted from the last call to Download.  Null if original URL matches final URL
        /// </summary>
        string ResponseURL();
    }
}
