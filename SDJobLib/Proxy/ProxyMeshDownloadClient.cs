﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SDJobLib.Proxy
{
    public class ProxyMeshDownloadClient : IDownloadClient
    {
        #region static configuration

        private static string _proxyAddress = SDJobs.GetProcessorSetting("ProxyMesh.Address");
        private static string _proxyUsername = SDJobs.GetProcessorSetting("ProxyMesh.Username");
        private static string _proxyPassword = SDJobs.GetProcessorSetting("ProxyMesh.Password");
        private static string _authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(string.Format("{0}:{1}", _proxyUsername, _proxyPassword)));
        private static int _timeout = Convert.ToInt32(SDJobs.GetProcessorSetting("ProxyMesh.Timeout"));

        #endregion

        #region state stuff

        private string _stringResult = null;
        public string StringResult()
        {
            return _stringResult;
        }

        private string _lastException = null;
        public string LastException()
        {
            return _lastException;
        }

        #endregion

        public ProxyMeshDownloadClient()
        {
        }

        private string _responseURL = null;
        public string ResponseURL()
        {
            return _responseURL;
        }

        public void Download(string url, int maxRetries)
        {
            try
            {

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

                //proxy stuff
                WebProxy proxy = new WebProxy(_proxyAddress, true);
                req.Proxy = proxy;
                req.Headers.Add(HttpRequestHeader.ProxyAuthorization, "Basic " + _authInfo);
                req.UserAgent = (new UserAgent()).GetRandomUserAgent();
                req.Timeout = _timeout;

                //other configuration
                req.AllowAutoRedirect = true;
                req.MaximumAutomaticRedirections = 10;

                using (WebResponse resp = req.GetResponse())
                {
                    string rUrl = resp.ResponseUri.AbsoluteUri.Trim();
                    if (rUrl.ToLower() != url.Trim().ToLower())
                        _responseURL = rUrl;

                    using (Stream stream = resp.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            _stringResult = reader.ReadToEnd();
                        }
                    }
                }


            }
            catch (WebException webex)
            {
                _lastException = webex.ToString();
            }
            catch (Exception ex)
            {
                _lastException = ex.ToString();
            }


        }

    }
}
