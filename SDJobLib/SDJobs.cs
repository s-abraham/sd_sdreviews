﻿using SDJobLib.JobDataObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDJobLib
{
    public static class SDJobs
    {
        private const int SAVE_NEW_JOB_BATCH_SIZE = 500;

        #region jobs database

        public static void SaveNewJob(BaseJobData data, int priority)
        {
            SaveNewJobs(new List<BaseJobData>() { data }, priority);
        }

        public static void SaveNewJobs(List<BaseJobData> items, int priority)
        {
            fastJSON.JSON.Instance.Parameters.UseExtensions = false;

            int counter = 0;
            StringBuilder sb = new StringBuilder();
            foreach (BaseJobData item in items)
            {
                string json = fastJSON.JSON.Instance.ToJSON(item);
                sb.AppendLine(String.Format("exec spAddNewJob {0},{1},'{2}'; ", (long)item.JobType(), priority, SafeSQL(json)));

                counter++;
                if (counter >= SAVE_NEW_JOB_BATCH_SIZE)
                {
                    counter = 0;
                    using (JobDBEntities db = new JobDBEntities())
                    {
                        db.exec(sb.ToString());
                        sb = new StringBuilder();
                    }
                }
            }
            if (counter > 0)
            {
                using (JobDBEntities db = new JobDBEntities())
                {
                    db.exec(sb.ToString());
                }
            }

        }

        public static List<Job> GetJobs(List<long> jobIDs)
        {
            using (JobDBEntities db = new JobDBEntities())
            {
                return (
                    from j in db.Jobs
                    where jobIDs.Contains(j.ID)
                    orderby j.Priority descending
                    select j
                ).ToList();
            }
        }

        public static void SetJobStatus(long jobID, JobStatusEnum status)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat(" update Jobs set JobStatus={0}", (long)status);
                if (status == JobStatusEnum.Processing)
                    sb.Append(",DateStarted=GetDate()");
                else if (status == JobStatusEnum.Success)
                    sb.Append(",DateCompleted=GetDate()");
                sb.AppendFormat(" where ID={0}", jobID);

                using (JobDBEntities db = new JobDBEntities())
                {
                    db.exec(sb.ToString());
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        public static void SetJobFailed(long jobID, string message, string exceptionText)
        {
            try
            {
                if (message == null) message = "";
                if (message.Length >= 200) message = message.Substring(0, 199);
                if (exceptionText == null) exceptionText = "";

                StringBuilder sb = new StringBuilder();
                sb.AppendFormat(" update Jobs set JobStatus={0}", (long)JobStatusEnum.Failure);
                sb.AppendFormat(" where ID={0}", jobID);

                sb.Append(" insert JobExceptions(JobID,Message,ExceptionText,DateCreated) ");
                sb.AppendFormat(" values ({0},'{1}','{2}',getdate()) ", jobID, SafeSQL(message), SafeSQL(exceptionText));

                using (JobDBEntities db = new JobDBEntities())
                {
                    db.exec(sb.ToString());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        public static string GetProcessorSetting(string key)
        {
            key = key.Trim().ToLower();
            using (JobDBEntities db = new JobDBEntities())
            {
                return (from s in db.ProcessorSettings where s.Name.Trim().ToLower() == key select s.Value).FirstOrDefault();
            }
        }

        #endregion

        #region socialdealer database

        public static List<int> GetReviewsToProcess(
            DownloadTypeEnum downloadType,
            int? dealerLocationID,
            int? reviewsSourceID,
            int? minMinutesSinceLastCompleted,
            int? minMinutesSinceLastQueued,
            bool onlyQueuedButNotCompleted,
            bool onlyNewOrSuccessful
        )
        {
            try
            {

                List<int> iDs = new List<int>();

                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("spGetReviewsToProcess @DownloadType={0}", (int)downloadType);

                if (dealerLocationID.HasValue)
                    sb.AppendFormat(",@DealerLocationID={0}", dealerLocationID.Value);

                if (reviewsSourceID.HasValue)
                    sb.AppendFormat(",@ReviewsSourceID={0}", reviewsSourceID.Value);

                if (minMinutesSinceLastCompleted.HasValue)
                    sb.AppendFormat(",@MinMinutesSinceLastCompleted={0}", minMinutesSinceLastCompleted.Value);

                if (minMinutesSinceLastQueued.HasValue)
                    sb.AppendFormat(",@MinMinutesSinceLastQueued={0}", minMinutesSinceLastQueued.Value);

                sb.AppendFormat(",@OnlyQueuedButNotCompleted={0}", onlyQueuedButNotCompleted ? 1 : 0);
                sb.AppendFormat(",@OnlyNewOrSuccessful={0}", onlyNewOrSuccessful ? 1 : 0);


                using (SocialDealerEntities db = new SocialDealerEntities())
                {
                    using (IDbCommand cmd = db.getCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sb.ToString();

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                iDs.Add(reader.GetInt32(0));
                            }
                        }
                    }
                }
                return iDs;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        public static List<DealerLocationReview> GetDealerLocationReviews(List<int> dealerLocationReviewIDs)
        {
            try
            {
                using (SocialDealerEntities db = new SocialDealerEntities())
                {
                    return (
                        from dlr in db.DealerLocationReviews
                        where dealerLocationReviewIDs.Contains(dlr.ID)
                        select dlr
                    ).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        public static List<int> GetActiveReviewSourceIDs()
        {
            using (SocialDealerEntities db = new SocialDealerEntities())
            {
                return (from dlr in db.DealerLocationReviews select dlr.ReviewsSourceID).Distinct().ToList();
            }
        }

        public static List<int> GetActiveDealerLocationIDs()
        {
            using (SocialDealerEntities db = new SocialDealerEntities())
            {
                return (from dlr in db.DealerLocationReviews select dlr.DealerLocationID).Distinct().ToList();
            }
        }

        #endregion

        #region utility functions

        private static string SafeSQL(string input)
        {
            return input.Replace("'", "''");
        }

        #endregion
    }
}
