﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDJobLib
{
    public partial class SocialDealerEntities : ObjectContext
    {
        public void exec(string sql)
        {
            ConnectionState state = this.Connection.State;

            if (state != System.Data.ConnectionState.Open)
                this.Connection.Open();

            try
            {
                using (IDbCommand cmd = this.Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = sql;
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        public IDbCommand getCommand()
        {
            DbConnection conn = ((EntityConnection)this.Connection).StoreConnection;
            if (conn.State != System.Data.ConnectionState.Open)
                conn.Open();

            return conn.CreateCommand();
        }
    }
}
