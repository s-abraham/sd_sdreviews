﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.EntityClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDJobLib
{
    public partial class JobDBEntities
    {
        public void exec(string sql)
        {
            
            try
            {
                using (IDbCommand cmd = getCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = sql;
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }

        public IDbCommand getCommand()
        {
            DbConnection conn = ((EntityConnection)this.Connection).StoreConnection;
            if (conn.State != System.Data.ConnectionState.Open)
                conn.Open();

            return conn.CreateCommand();
        }
    }
}
