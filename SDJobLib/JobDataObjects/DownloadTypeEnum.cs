﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDJobLib
{
    public enum DownloadTypeEnum : int
    {        
        SummaryAndDetail = 1,
        SummaryAndDetailIfChanged = 2,
        SummaryOnly = 3
    }
}
