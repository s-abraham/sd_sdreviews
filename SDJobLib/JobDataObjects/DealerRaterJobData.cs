﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDJobLib.JobDataObjects
{
    public class DealerRaterJobData : BaseJobData
    {
        public int DealerLocationReviewID { get; set; }
        public int DealerLocationID { get; set; }    
        public int ReviewSourceID { get; set; }

        public DownloadTypeEnum Type { get; set; }
        public int MaxRetries { get; set; }

        public string URL { get; set; }

        public override JobTypeEnum JobType()
        {
            return JobTypeEnum.DealerRaterScrape;
        }
    }
}
