﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDJobLib.JobDataObjects
{
    public abstract class BaseJobData
    {
        public abstract JobTypeEnum JobType();
    }
}
