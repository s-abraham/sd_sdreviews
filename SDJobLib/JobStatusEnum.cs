﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDJobLib
{
    public enum JobStatusEnum : long
    {
        Idle = 0,
        AssignedToController = 100,
        AssignedToProcess = 200,
        Processing = 300,
        Success = 400,
        Failure = 500
    }
}
